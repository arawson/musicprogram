
package com.debugarcade.mp.model;

/**
 *
 * @author arawson
 */
public class Note {
    private final int noteNumber;

    public Note(int noteNumber) {
        this.noteNumber = noteNumber;
    }

    public int getNoteNumber() {
        return noteNumber;
    }
}
